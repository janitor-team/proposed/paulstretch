paulstretch (2.2-2-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org
  * d/rules: Remove trailing whitespaces
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Olivier Humbert ]
  * Update copyright (http -> https)

  [ Sudip Mukherjee ]
  * Fix FTBFS. (Closes: #952055)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Wed, 08 Apr 2020 17:39:11 +0100

paulstretch (2.2-2-4) unstable; urgency=medium

  * Set dh/compat 10.
  * Bump Standards.
  * Fix VCS fields.
  * Add Keywords to desktop file.
  * Remove menu file.
  * Fix some hardening.
  * Make build reproducible. (Closes: #842583)
  * Add spelling patch.
  * Pass the build flags.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 30 Oct 2016 20:49:07 +0100

paulstretch (2.2-2-3) unstable; urgency=medium

  * Team upload.
  * debian/rules:
    - Also link with vorbis and ogg. (Closes: #768729)
    - Build in override_dh_auto_build and not in override_dh_auto_install.

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 11 Nov 2014 13:33:39 +0100

paulstretch (2.2-2-2) unstable; urgency=low

  * Team upload.
  * Add dependency on jackd.
    Thanks to Jérémy Lal for the report. (Closes: #663187)
  * Set Vcs tags to Debian packaging repository.

 -- Alessio Treglia <alessio@debian.org>  Sun, 20 May 2012 23:59:59 +0200

paulstretch (2.2-2-1) unstable; urgency=low

  * Initial release. (Closes: #594784)

 -- Robin Gareus <robin@gareus.org>  Tue, 7 Mar 2012 00:52:30 +0100
